
const React = require('react-native');

const { StyleSheet } = React;

module.exports = StyleSheet.create({
  sidebar: {
    flex: 1,
      paddingLeft: -20,

      width: 300,
      backgroundColor: '#F57E57',
  },
    menuItem: {borderColor: 'white'},
    menuItemText: {color: 'white'}
});
