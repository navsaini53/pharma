
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Content, Text, List, ListItem, Icon} from 'native-base';

import { setIndex } from '../../actions/list';
import navigateTo from '../../actions/sideBarNav';
import myTheme from '../../themes/base-theme';

import styles from './style';

class SideBar extends Component {

  static propTypes = {

    navigateTo: React.PropTypes.func,
  }

  navigateTo(route) {
    this.props.navigateTo(route, 'home');
  }

  render() {
    return (

        <Content style={styles.sidebar} theme={myTheme}>
        <List>
            <ListItem style={styles.menuItem} iconRight button onPress={() => this.navigateTo('home')}>
                <Text style={styles.menuItemText}>Home</Text>
                <Icon style={styles.menuItemText} name="ios-home-outline"/>
          </ListItem>
            <ListItem style={styles.menuItem} iconRight button onPress={() => this.navigateTo('acepic')}>
                <Text style={styles.menuItemText}>Acepic-SP</Text>
                <Icon style={styles.menuItemText} name="ios-medkit-outline"/>
          </ListItem>

            <ListItem style={styles.menuItem} iconRight button onPress={() => this.navigateTo('acerbit')}>
                <Text style={styles.menuItemText}>Acerbit-DSR</Text>
                <Icon style={styles.menuItemText} name="ios-medkit-outline"/>
            </ListItem>
            <ListItem style={styles.menuItem} iconRight button onPress={() => this.navigateTo('avunateBase')}>
                <Text style={styles.menuItemText}>Avunate-625</Text>
                <Icon style={styles.menuItemText} name="ios-medkit-outline"/>
            </ListItem>
            <ListItem style={styles.menuItem} iconRight button onPress={() => this.navigateTo('foldyBase')}>
                <Text style={styles.menuItemText}>Foldy-Plus</Text>
                <Icon style={styles.menuItemText} name="ios-medkit-outline"/>
            </ListItem>
            <ListItem style={styles.menuItem} iconRight button onPress={() => this.navigateTo('inusXT')}>
                <Text style={styles.menuItemText}>Inus-XT</Text>
                <Icon style={styles.menuItemText} name="ios-medkit-outline"/>
            </ListItem>

            <ListItem style={styles.menuItem} iconRight button onPress={() => this.navigateTo('vitKind')}>
                <Text style={styles.menuItemText}>Vitkind</Text>
                <Icon style={styles.menuItemText} name="ios-medkit-outline"/>
            </ListItem>
            <ListItem style={styles.menuItem} iconRight button onPress={() => this.navigateTo('etoxy')}>
                <Text style={styles.menuItemText}>Etoxy-90</Text>
                <Icon style={styles.menuItemText} name="ios-medkit-outline"/>
            </ListItem>


        </List>
      </Content>

    );
  }
}

function bindAction(dispatch) {
  return {
    setIndex: index => dispatch(setIndex(index)),
    navigateTo: (route, homeRoute) => dispatch(navigateTo(route, homeRoute)),
  };
}

const mapStateToProps = state => ({
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(SideBar);
