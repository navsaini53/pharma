const React = require('react-native');
import {Dimensions} from 'react-native';

const {StyleSheet} = React;
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);
module.exports = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1
    },

});
