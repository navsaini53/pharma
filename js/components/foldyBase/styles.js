const React = require('react-native');
import {Dimensions} from 'react-native';

const {StyleSheet} = React;
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);
module.exports = StyleSheet.create({
    container: {
        backgroundColor: 'white', flex: 1
    },
    lineImg: {height: 100, position: 'absolute', top: 10, width: vw * 100, resizeMode: 'cover'},
    headerImg: {height: vh * 95, width: vw * 100, resizeMode: 'cover'},
    leftImg: {height: vh * 65, width: vw * 35, resizeMode: 'cover'},
    rightImg: {height: vh * 65, width: vw * 65, resizeMode: 'cover'},

});
