const React = require('react-native');
import {Dimensions} from 'react-native';

const {StyleSheet} = React;
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);

module.exports = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,


        alignItems: 'center',

    },
    headerImg: {
        height: vh * 23, width: vw * 100,
        resizeMode: 'contain'
    },
    leftImg: {height: vh * 40, width: vw * 100, resizeMode: 'cover'},
    rightImg: {
        height: vh * 40, position: 'absolute', right: 0, bottom: -20,
        width: vw * 50, resizeMode: 'contain'
    },
    packat: {
        height: 140, position: 'absolute', right: 20, top: 20,
        width: 100, resizeMode: 'contain'
    },
    orangeLine: {flexDirection: 'column', height: vh * 20, alignItems: 'center'},
    orangeLineOne: {
        height: vh * 10, width: vw * 100,
        resizeMode: 'cover'
    },
    whiteLine: {
        height: vh * 10, width: vw * 50,
        resizeMode: 'contain'
    },

    icon: {fontSize: 50, color: 'black'},
    leftBtn: {position: 'absolute', top: 150, left: 15}


});
