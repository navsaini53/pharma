import React, {Component} from 'react';
import {TouchableOpacity, TouchableWithoutFeedback, Image, Dimensions, LayoutAnimation, Animated} from 'react-native';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {Container, Header, Title, Content, Text, Button, Icon, View} from 'native-base';
import {Grid, Row} from 'react-native-easy-grid';
import myTheme from '../../themes/base-theme';
import * as Animatable from 'react-native-animatable';
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);
import {openDrawer} from '../../actions/drawer';
import {setIndex} from '../../actions/list';
import styles from './styles';
import Footer from '../footer/index';
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';


const {
    reset,
    pushRoute,
    popRoute
} = actions;

class VitKindFinal extends Component {

    static propTypes = {
        name: React.PropTypes.string,
        list: React.PropTypes.arrayOf(React.PropTypes.string),
        setIndex: React.PropTypes.func,
        openDrawer: React.PropTypes.func,
        pushRoute: React.PropTypes.func,
        reset: React.PropTypes.func,
        navigation: React.PropTypes.shape({
            key: React.PropTypes.string,
        }),
    }

    pushRoute(route, index) {
        this.props.setIndex(index);
        this.props.pushRoute({key: route, index: 1}, this.props.navigation.key);
    }


    constructor(props) {
        super(props);
        this.state = {
            animatedVal: new Animated.Value(100),
        };
    }

    componentWillMount() {


    }


    onSwipeLeft(gestureState) {

        this.props.pushRoute({key: 'etoxy', index: 1}, this.props.navigation.key);

    }

    onSwipeRight(gestureState) {

        this.props.popRoute(this.props.navigation.key);

    }


    render() {
        const config = {
            velocityThreshold: 0.1,
            directionalOffsetThreshold: 50
        };
        return (
            <GestureRecognizer

                onSwipeLeft={(state) => this.onSwipeLeft(state)}
                onSwipeRight={(state) => this.onSwipeRight(state)}
                config={config} style={{
                flex: 1,

            }}>
                <Container style={styles.container}>

                    <Content>
                        <View>
                            <Animatable.Image animation="slideInLeft" duration={1000}
                                              style={{height: vh * 25, width: vw * 100, resizeMode: 'center'}}
                                              source={require('../../../images/ppt/11/1.png')}/>
                            <Animatable.Image animation="slideInRight" duration={1000}
                                              style={{height: vh * 15, width: vw * 100, resizeMode: 'center'}}
                                              source={require('../../../images/ppt/11/2.png')}/>
                            <View style={{flexDirection: 'row'}}>
                                <Animatable.Image animation="slideInUp" delay={500} duration={1000}
                                                  style={{height: vh * 45, width: vw * 50, resizeMode: 'center'}}
                                                  source={require('../../../images/ppt/11/4.png')}/>
                                <Animatable.Image animation="flipInY" delay={700} duration={1500}
                                                  style={{
                                                      height: vh * 40,
                                                      marginTop: 10,
                                                      width: vw * 50,
                                                      resizeMode: 'center'
                                                  }}
                                                  source={require('../../../images/ppt/11/3.png')}/>

                            </View>


                        </View>


                    </Content>

                </Container>
                <Footer/>

            </GestureRecognizer>
        );
    }
}

function bindAction(dispatch) {
    return {
        setIndex: index => dispatch(setIndex(index)),
        openDrawer: () => dispatch(openDrawer()),
        popRoute: key => dispatch(popRoute(key)),
        pushRoute: (route, key) => dispatch(pushRoute(route, key)),
        reset: key => dispatch(reset([{key: 'login'}], key, 0)),
    };
}

const mapStateToProps = state => ({
    name: state.user.name,
    list: state.list.list,
    navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(VitKindFinal);
