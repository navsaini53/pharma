
import React, { Component } from 'react';
import {TouchableOpacity, TouchableWithoutFeedback, Image, Dimensions, LayoutAnimation, Animated} from 'react-native';
import { connect } from 'react-redux';
import { actions } from 'react-native-navigation-redux-helpers';
import {Container, Header, Title, Content, Text, Button, Icon, View} from 'native-base';
import { Grid, Row } from 'react-native-easy-grid';
import myTheme from '../../themes/base-theme';
import * as Animatable from 'react-native-animatable';
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);
import { openDrawer } from '../../actions/drawer';
import { setIndex } from '../../actions/list';
import styles from './styles';
import Footer from '../footer/index';
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';
const {
  reset,
  pushRoute,
} = actions;

class Home extends Component {

  static propTypes = {
    name: React.PropTypes.string,
    list: React.PropTypes.arrayOf(React.PropTypes.string),
    setIndex: React.PropTypes.func,
    openDrawer: React.PropTypes.func,
    pushRoute: React.PropTypes.func,
    reset: React.PropTypes.func,
    navigation: React.PropTypes.shape({
      key: React.PropTypes.string,
    }),
  }

  pushRoute(route, index) {
    this.props.setIndex(index);
    this.props.pushRoute({ key: route, index: 1 }, this.props.navigation.key);
  }


  constructor(props) {
    super(props);
    this.state = {
      animatedVal: new Animated.Value(100),
    };
  }


  onSwipeLeft(gestureState) {

    this.props.pushRoute({key: 'acepic', index: 1}, this.props.navigation.key);

  }



  render() {

    const config = {
      velocityThreshold: 0.1,
      directionalOffsetThreshold: 50
    };
    return (
        <GestureRecognizer


            onSwipeLeft={(state) => this.onSwipeLeft(state)}
            config={config} style={styles.container}>

          <Animatable.Image animation="zoomIn" delay={200}
                            duration={1000} style={styles.baseImage} source={require('../../../images/ppt/1.png')}/>
          <Footer/>
        </GestureRecognizer>

    );
  }
}

function bindAction(dispatch) {
  return {
    setIndex: index => dispatch(setIndex(index)),
    openDrawer: () => dispatch(openDrawer()),
    pushRoute: (route, key) => dispatch(pushRoute(route, key)),
    reset: key => dispatch(reset([{ key: 'login' }], key, 0)),
  };
}

const mapStateToProps = state => ({
  name: state.user.name,
  list: state.list.list,
  navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(Home);
