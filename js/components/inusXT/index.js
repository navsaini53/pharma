import React, {Component} from 'react';
import {TouchableOpacity, TouchableWithoutFeedback, Image, Dimensions, LayoutAnimation, Animated} from 'react-native';
import {connect} from 'react-redux';
import {actions} from 'react-native-navigation-redux-helpers';
import {Container, Header, Title, Content, Text, Button, Icon, View} from 'native-base';
import {Grid, Row} from 'react-native-easy-grid';
import myTheme from '../../themes/base-theme';
import * as Animatable from 'react-native-animatable';
const {width, height, scale} = Dimensions.get("window"),
    vw = width / 100,
    vh = height / 100,
    vmin = Math.min(vw, vh),
    vmax = Math.max(vw, vh);
import {openDrawer} from '../../actions/drawer';
import {setIndex} from '../../actions/list';
import styles from './styles';
import Footer from '../footer/index';

import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';


const {
    reset,
    pushRoute,
    popRoute
} = actions;

class InusXT extends Component {

    static propTypes = {
        name: React.PropTypes.string,
        list: React.PropTypes.arrayOf(React.PropTypes.string),
        setIndex: React.PropTypes.func,
        openDrawer: React.PropTypes.func,
        pushRoute: React.PropTypes.func,
        reset: React.PropTypes.func,
        navigation: React.PropTypes.shape({
            key: React.PropTypes.string,
        }),
    }

    pushRoute(route, index) {
        this.props.setIndex(index);
        this.props.pushRoute({key: route, index: 1}, this.props.navigation.key);
    }


    constructor(props) {
        super(props);
        this.state = {
            animatedVal: new Animated.Value(100),
        };
    }

    componentWillMount() {


    }


    onSwipeLeft(gestureState) {

        this.props.pushRoute({key: 'vitKind', index: 1}, this.props.navigation.key);

    }

    onSwipeRight(gestureState) {

        this.props.popRoute(this.props.navigation.key);

    }


    render() {
        const config = {
            velocityThreshold: 0.1,
            directionalOffsetThreshold: 50
        };
        return (
            <GestureRecognizer

                onSwipeLeft={(state) => this.onSwipeLeft(state)}
                onSwipeRight={(state) => this.onSwipeRight(state)}
                config={config} style={{
                flex: 1,

            }}>
                <Container style={styles.container}>

                    <Content>

                        <View style={{flexDirection: 'row', flex: 1}}>
                            <View style={{flex: 3}}>
                                <Animatable.Image animation="slideInLeft" delay={300} duration={1000}
                                                  style={{height: vh * 30, width: vw * 70, resizeMode: 'cover'}}
                                                  source={require('../../../images/ppt/9/2.png')}/>
                                <Animatable.Image animation="slideInLeft" delay={500} duration={1000}
                                                  style={{height: vh * 60, width: vw * 70, resizeMode: 'stretch'}}
                                                  source={require('../../../images/ppt/9/6.png')}/>

                                <Animatable.Image animation="zoomIn" delay={600} duration={1000}
                                                  style={{
                                                      height: 70, width: vw * 70,
                                                      resizeMode: 'center', position: 'absolute', top: 190, left: -10
                                                  }}
                                                  source={require('../../../images/ppt/9/3.png')}/>
                                <Animatable.Image animation="slideInLeft" delay={500} duration={1000}
                                                  style={{
                                                      height: 35, width: vw * 70,
                                                      resizeMode: 'center', position: 'absolute', top: 263
                                                  }}
                                                  source={require('../../../images/ppt/9/4.png')}/>

                                <Animatable.Image animation="slideInLeft" delay={500} duration={1000}
                                                  style={{
                                                      height: 10, width: vw * 70,
                                                      resizeMode: 'center', position: 'absolute', top: 300
                                                  }}
                                                  source={require('../../../images/ppt/9/5.png')}/>


                            </View>
                            <View style={{marginLeft: -25}}>
                                <Animatable.Image animation="slideInDown" delay={500} duration={1000}
                                                  style={{height: vh * 90, width: vw * 30, resizeMode: 'cover'}}
                                                  source={require('../../../images/ppt/9/1.png')}/>
                            </View>


                        </View>
                        {/* <View>
                         <Animatable.Image style={{
                         position:'absolute',bottom:23,height:40,width:vw*100,
                         resizeMode: 'cover'
                         }}

                         source={require('../../../images/ppt/footer.png')}/>
                         </View>
                         */}


                    </Content>

                </Container>
                <Footer/>
            </GestureRecognizer>
        );
    }
}

function bindAction(dispatch) {
    return {
        setIndex: index => dispatch(setIndex(index)),
        openDrawer: () => dispatch(openDrawer()),
        popRoute: key => dispatch(popRoute(key)),
        pushRoute: (route, key) => dispatch(pushRoute(route, key)),
        reset: key => dispatch(reset([{key: 'login'}], key, 0)),
    };
}

const mapStateToProps = state => ({
    name: state.user.name,
    list: state.list.list,
    navigation: state.cardNavigation,
});

export default connect(mapStateToProps, bindAction)(InusXT);
